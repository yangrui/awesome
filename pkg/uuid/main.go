package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"hash/crc32"

	"errors"

	"github.com/google/uuid"
)

func main() {
	Errr()
	//fmt.Println(GenerateUUIDNumber())
}

func GenerateUUIDNumber() int32 {
	var res int32
	newuuid := uuid.New()
	fmt.Println(newuuid.String())
	bf, _ := newuuid.MarshalBinary()
	fmt.Println(crc32.ChecksumIEEE(bf))
	buf := bytes.NewReader(bf)
	_ = binary.Read(buf, binary.LittleEndian, &res)
	return res
}

func Errr() {
	err := errors.New("error")
	println(err.Error())
	err = nil
	println(err.Error())
}
