/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"net"
	"os"
	"strconv"
)

var (
	ipAddress string
	port      uint
)

func init() {
	//cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&ipAddress, "ip", "127.0.0.1", "bind ip address default 127.0.0.1")
	rootCmd.PersistentFlags().UintVar(&port, "port", uint(9090), "bind port default 9090")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tcplisten",
	Short: "open tcp port and print out",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd-tcplisten *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	p := 9090
	if port < 65535 && port > 0 {
		p = int(port)
	}
	bindobject := ipAddress + ":" + strconv.Itoa(p)
	fmt.Println("bind: ", bindobject)
	tl, err := net.Listen("tcp", bindobject)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	for {
		conn, err := tl.Accept()
		if err != nil {
			fmt.Println(err)
			os.Exit(3)
		}
		go dealConnect(conn)
	}
}

func dealConnect(conn net.Conn) {
	fmt.Println("connect ", conn.RemoteAddr())
	defer conn.Close()

	for {
		buf := make([]byte, 65535)
		_, err := conn.Read(buf)
		if err != nil {
			fmt.Println(err)
			os.Exit(4)
		}
		fmt.Println(string(buf))
		_, err = conn.Write([]byte(`ok`))
		if err != nil {
			fmt.Println(err)
			os.Exit(5)
		}
	}
}

//
//// initConfig reads in config file and ENV variables if set.
//func initConfig() {
//	if cfgFile != "" {
//		// Use config file from the flag.
//		viper.SetConfigFile(cfgFile)
//	} else {
//		// Find home directory.
//		home, err := homedir.Dir()
//		if err != nil {
//			fmt.Println(err)
//			os.Exit(1)
//		}
//
//		// Search config in home directory with name ".tcplisten" (without extension).
//		viper.AddConfigPath(home)
//		viper.SetConfigName(".tcplisten")
//	}
//
//	viper.AutomaticEnv() // read in environment variables that match
//
//	// If a config file is found, read it in.
//	if err := viper.ReadInConfig(); err == nil {
//		fmt.Println("Using config file:", viper.ConfigFileUsed())
//	}
//}
