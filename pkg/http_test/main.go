package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {

	e := echo.New()
	e.GET("/traefik", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"code": 200,
		})
	})
	e.Logger.Fatal(e.Start(":9999"))
}
