package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	type Test struct {
		Int    int    `json:"int,omitempy"`
		String string `json:"string"`
	}
	a := Test{}
	b, _ := json.Marshal(a)
	fmt.Println(string(b))

	ctx := make(chan int, 2)
	bb, err := json.Marshal(ctx)
	if err != nil {
		panic(err)
	}
	fmt.Println(bb)

}
