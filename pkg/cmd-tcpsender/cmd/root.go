/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bufio"
	"fmt"
	"github.com/spf13/cobra"
	"net"
	"os"
	"strconv"
)

var (
	ipAddress string
	port      uint
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tcpsender",
	Short: "connect tcp and sender text",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd-tcplisten *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	p := 9090
	if port < 65535 && port > 0 {
		p = int(port)
	}
	target := ipAddress + ":" + strconv.Itoa(p)
	fmt.Println("target : ", target)

	conn, err := net.Dial("tcp", target)

	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	defer conn.Close()
	buf := bufio.NewReader(os.Stdin)

	for {

		d, err := buf.ReadBytes('\n')
		if err != nil {
			fmt.Println(err)
			os.Exit(3)
		}
		l := len(d)
		d = d[:l-1]
		fmt.Println("input:", string(d))
		_, err = conn.Write(d)
		if err != nil {
			fmt.Println(err)
			os.Exit(4)
		}
		resp := make([]byte, 2)
		_, err = conn.Read(resp)
		if err != nil {
			fmt.Println(err)
			os.Exit(5)
		}
		fmt.Println(string(resp))
	}

}

func init() {
	//cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&ipAddress, "ip", "127.0.0.1", "bind ip address default 127.0.0.1")
	rootCmd.PersistentFlags().UintVar(&port, "port", uint(9090), "bind port default 9090")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
