package main

import "fmt"

/*
152. 乘积最大子数组
给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。



示例 1:

输入: [2,3,-2,4]
输出: 6
解释: 子数组 [2,3] 有最大乘积 6。
示例 2:

输入: [-2,0,-1]
输出: 0
解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
*/
func main() {
	a := []int{2, -1, 6, -2, 0, 88}
	fmt.Println(maxProduct(a))
}

func maxProduct(nums []int) int {
	ma, mi, rst := nums[0], nums[0], nums[0]
	for i := 1; i < len(nums); i++ {
		mx, mn := ma, mi
		ma = max(mx*nums[i], max(nums[i], nums[i]*mn))
		mi = min(mx*nums[i], min(nums[i], nums[i]*mn))
		rst = max(rst, ma)
	}

	return ma
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
