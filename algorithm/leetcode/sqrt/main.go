package main

/*
29. 两数相除
给定两个整数，被除数 dividend 和除数 divisor。将两数相除，要求不使用乘法、除法和 mod 运算符。

返回被除数 dividend 除以除数 divisor 得到的商。

整数除法的结果应当截去（truncate）其小数部分，例如：truncate(8.345) = 8 以及 truncate(-2.7335) = -2



示例 1:

输入: dividend = 10, divisor = 3
输出: 3
解释: 10/3 = truncate(3.33333..) = truncate(3) = 3
示例 2:

输入: dividend = 7, divisor = -3
输出: -2
解释: 7/-3 = truncate(-2.33333..) = -2


提示：

被除数和除数均为 32 位有符号整数。
除数不为 0。
假设我们的环境只能存储 32 位有符号整数，其数值范围是 [−231,  231 − 1]。本题中，如果除法结果溢出，则返回 231 − 1。
*/

func divide(dividend int, divisor int) int {
	dd, dv, flag := judge(dividend, divisor)
	rst := div(dd, dv)
	if flag {
		rst = 0 - rst
	}
	if rst < (0-(1<<31)) || rst > (1<<31-1) {
		return 1<<31 - 1
	}
	return rst
}

func judge(dividend int, divisor int) (dd, dv int, flag bool) {
	if dividend < 0 {
		flag = !flag
		dd = 0 - dividend
	} else {
		dd = dividend
	}
	if divisor < 0 {
		flag = !flag
		dv = 0 - divisor
	} else {
		dv = divisor
	}
	return
}

func div(a, b int) int {
	cur := 0
	if a < b {
		return cur
	}
	cur = 1
	tmp := b
	for {
		tmp = tmp << 1
		if tmp > a {
			cur += div(a-(tmp>>1), b)
			break
		}
		cur = cur << 1
	}
	return cur
}
