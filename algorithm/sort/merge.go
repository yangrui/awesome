package sort

import "fmt"

func MergeSort(a []int) []int {
	if len(a) < 2 {
		return a
	}
	mid := len(a) / 2
	l := MergeSort(a[0:mid])
	r := MergeSort(a[mid:])
	result := Merge(l, r)
	return result

}

func Merge(a, b []int) []int {
	l1, l2 := len(a), len(b)
	res := make([]int, 0)
	i, j := 0, 0
	for i < l1 && j < l2 {
		if a[i] < b[j] {
			res = append(res, a[i])
			i++
			continue
		}
		res = append(res, b[j])
		j++
	}
	res = append(res, a[i:]...)
	res = append(res, b[j:]...)
	fmt.Println(res)
	return res
}
