package sort

import (
	"fmt"
	"math/rand"
	"testing"
)

func Test_quickSort(t *testing.T) {
	a := make([]int, 30)
	for i, _ := range a {
		a[i] = rand.Intn(99)
	}
	res := quicksort(a)

	fmt.Println(res)
}
