package sort

import (
	"math/rand"
)

func QuickSort(a []int) []int {
	if len(a) < 2 {
		return a
	}

	i, j := 1, len(a)-1
	for i < j {
		if a[i] > a[0] {
			a[i], a[j] = a[j], a[i]
			j--
			continue
		}
		i++
	}
	//fmt.Println(a[0], a[j])
	if a[0] > a[i] {
		a[0], a[i] = a[i], a[0]
	}
	QuickSort(a[:i])
	QuickSort(a[i:])
	return a
}

func quicksort(a []int) []int {
	if len(a) < 2 {
		return a
	}

	left, right := 0, len(a)-1

	pivot := rand.Int() % len(a)

	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		if a[i] < a[right] {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	quicksort(a[:left])
	quicksort(a[left+1:])

	return a
}
