package sort

import "fmt"

func HeapSort(a []int) {
	l := (len(a) - 1) >> 1
	for i := l; i >= 0; i-- {
		fixHeap(a, i, len(a)-1)
	}
	for i := len(a) - 1; i > 0; i-- {
		a[0], a[i] = a[i], a[0]
		fixHeap(a, 0, i-1)
	}
}

func fixHeap(a []int, start, end int) {
	le := 2*start + 1
	re := 2*start + 2
	if le > end {
		return
	}
	tmp := le
	if re < end && a[re] > a[le] {
		tmp = re
	}
	if a[start] < a[tmp] {
		a[start], a[tmp] = a[tmp], a[start]
		fixHeap(a, tmp, end)
	}
}

func heapsort(array []int) {
	ep := (len(array) - 1) >> 1
	fmt.Println(ep)
	for i := ep; i >= 0; i-- {
		heapt(array, i, len(array)-1)
	}

	for i := len(array) - 1; i > 0; i-- {
		array[0], array[i] = array[i], array[0]
		heapt(array, 0, i-1)
	}
}

func heapt(array []int, start int, end int) {
	le := start*2 + 1
	re := le + 1
	if le > end {
		return
	}

	var tmp = le
	if re <= end && array[re] > array[le] {
		tmp = re
	}

	if array[tmp] > array[start] {
		fmt.Println(start, end, array)
		array[start], array[tmp] = array[tmp], array[start]
		heapt(array, tmp, end)
	}
}
