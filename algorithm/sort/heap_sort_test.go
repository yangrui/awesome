package sort

import (
	"fmt"
	"math/rand"
	"testing"
)

func Test_heapSort(t *testing.T) {
	a := make([]int, 30)
	for i, _ := range a {
		a[i] = rand.Intn(99)
	}
	HeapSort(a)
	fmt.Println(a)
}
