package sort

import (
	"fmt"
	"math/rand"
	"testing"
)

func Test_mergeSort(t *testing.T) {
	a := make([]int, 30)
	for i, _ := range a {
		a[i] = rand.Intn(99)
	}
	res := MergeSort(a)
	fmt.Println(res)
}
