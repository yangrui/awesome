package main

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {

	mongoURI := fmt.Sprintf("mongodb://%s:%s@%s/chain_info?w=majority", "rui", "123456", "192.168.0.52")
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	err = client.Connect(ctx)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	db := client.Database("chain_info")
	c := db.Collection("message_detail")

	type SP struct {
		//	Bigint types.BigInt  `json:"BigInt" bson:"big_int"`
		Fil float64 `json:"fil" bson:"fil"`
	}
	sp := SP{654}

	fmt.Println(bson.Marshal(sp))

	is, err := c.InsertOne(context.Background(), sp)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(is.InsertedID)

	a := 0
	fmt.Println(a)
}
