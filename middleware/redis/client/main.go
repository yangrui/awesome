package main

import (
	"fmt"
	"github.com/go-redis/redis/v7"
	"net"
)

func main() {
	Subcribe()
}

func Subcribe() {
	cli := redis.NewClient(&redis.Options{
		Addr:     "192.168.5.130:6379",
		PoolSize: 1,
	})
	defer cli.Close()
	err := cli.Ping().Err()
	if err != nil {
		println(err.Error())
		return
	}
	cli.HSet("test", "asda", 1)
	fmt.Println(cli.HGetAll("test").Result())
	ip, err := externalIP()
	if err != nil {
		panic(err)
	}
	for _, v := range ip {
		fmt.Println(v.String())
	}
	return

	//time.Sleep(time.Second * 360)

}

type test struct {
	A int `json:"a,omitempty"`
}

func externalIP() ([]net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	ips := []net.IP{}
	for _, iface := range ifaces {
		fmt.Println(iface.Addrs())
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}

		for _, addr := range addrs {
			fmt.Println(addr)
			ip := getIpFromAddr(addr)
			if ip == nil {
				continue
			}
			ips = append(ips, ip)
		}
	}
	return ips, nil
}

func getIpFromAddr(addr net.Addr) net.IP {
	fmt.Println(addr.String())
	var ip net.IP
	switch v := addr.(type) {
	case *net.IPNet:
		ip = v.IP
	case *net.IPAddr:
		ip = v.IP
	}
	if ip == nil || ip.IsLoopback() {
		return nil
	}
	ip = ip.To4()
	if ip == nil {
		return nil // not an ipv4 address
	}

	return ip
}
