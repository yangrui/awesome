package main

/*
#include <stdio.h>
#include <stdlib.h>
int add(int a, int b)
{
	return a+b;
}

// 分配内存， 可以分配大于2G内存的 slice
void* makeslice(size_t memsize) {
    return malloc(memsize);
}
*/
import "C"
import (
	"fmt"
	"github.com/go-redis/redis/v7"
	"reflect"
	"time"
	"unsafe"
)

func main() {
	//Pubscibe()
	//Sunsafe()
	// Cgogo()
	var aint Aint
	aint = Aint{3, 2, 1}
	heapSort(aint)
	fmt.Println(aint)
}

type Aint []int

func (a Aint) Len() int {
	return len(a)
}
func (a Aint) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
func (a Aint) Less(i, j int) bool {
	return a[i] < a[j]
}

/*
Len() int
	// Less reports whether the element with
	// index i should sort before the element with index j.
	Less(i, j int) bool
	// Swap swaps the elements with indexes i and j.
	Swap(i, j int)
*/

func Cgogo() {
	C.puts(C.CString("asdasdas"))
	fmt.Println(reflect.TypeOf(C.add(2, 2)))
	fmt.Println(int(C.add(2, 3)))
	p := C.makeslice(1 << 31)
	fmt.Println(reflect.TypeOf(p))
	defer C.free(p)
	b := (*[1 << 31]byte)(p)
	fmt.Println(len(*b))
}
func Pubscibe() {
	rdb := redis.NewClient(&redis.Options{Addr: "127.0.0.1:6379"})
	defer rdb.Close()
	for i := 0; i < 30; i++ {
		rdb.Publish("qwe", i)
		time.Sleep(time.Second)
	}

}

func heapSort(a []int) {
	for i := 0; i < len(a); i++ {
		Heapify(a[i:])
	}
}

func Heapify(a []int) {
	l := len(a)
	if l == 0 {
		return
	}
	for m := l/2 - 1; m >= 0; m-- {
		left, right := 2*m, 2*m+1
		if left < l && a[left] < a[m] {
			a[m], a[left] = a[left], a[m]
		}
		if right < l && a[right] < a[m] {
			a[m], a[right] = a[right], a[m]
		}
		if right < l && left < l && a[right] < a[left] {
			a[right], a[left] = a[left], a[right]
		}
		fmt.Println(a)
	}
}

func Sunsafe() {
	var a int = 97
	arr := string(a)
	fmt.Println(arr)
	fmt.Println(reflect.TypeOf(arr))
	size := len(arr)
	p := uintptr(unsafe.Pointer(&arr))

	var data []byte

	sh := (*reflect.SliceHeader)(unsafe.Pointer(&data))
	sh.Data = p
	sh.Len = size
	sh.Cap = size
	fmt.Println(data)

}
