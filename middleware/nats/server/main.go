package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
)

func main() {
	nc, err := nats.Connect("localhost:4222")
	if err != nil {
		panic(err)
	}

	fmt.Println(nc.GetClientID())
	// Simple Publisher
	err = nc.Publish("topic", []byte("Hello World"))
	if err != nil {
		panic(err)
	}
	nc.PublishRequest()

	nc.Drain()
	nc.Flush()

	//// Simple Async Subscriber
	//nc.Subscribe("foo", func(m *nats.Msg) {
	//	fmt.Printf("Received a message: %s\n", string(m.Data))
	//})
	//
	//// Responding to a request message
	//nc.Subscribe("request", func(m *nats.Msg) {
	//	m.Respond([]byte("answer is 42"))
	//})
	//
	//
	//
	//// Simple Sync Subscriber
	//sub, err := nc.SubscribeSync("foo")
	////m, err := sub.NextMsg(time.Second*3)
	////if err != nil {
	////	panic(err)
	////}
	////fmt.Println(string(m.Data))
	//// Channel Subscriber
	//ch := make(chan *nats.Msg, 64)
	//sub, err = nc.ChanSubscribe("foo", ch)
	//if err != nil {
	//	panic(err)
	//}
	//msg := <- ch
	//fmt.Println(string(msg.Data))
	//// Unsubscribe
	//sub.Unsubscribe()
	//
	//// Drain
	//sub.Drain()
	//
	//// Requests
	//msg, err = nc.Request("help", []byte("help me"), 10*time.Millisecond)
	//if err != nil {
	//	panic(err)
	//}
	//
	//// Replies
	//nc.Subscribe("help", func(m *nats.Msg) {
	//	nc.Publish(m.Reply, []byte("I can help!"))
	//})

	// Drain connection (Preferred for responders)
	// Close() not needed if this is called.
	nc.Drain()

	// Close connection
	nc.Close()
}
