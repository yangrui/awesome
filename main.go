// Copyright 2010 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var cc int64

func main() {
	testaRand()
}

func asd() {
	wg := &sync.WaitGroup{}
	count := 95
	wg.Add(count)
	for i := 0; i < count; i++ {
		go func(tmp int64) {
			atomic.StoreInt64(&cc, tmp)
			if tmp > 90 {
				println(tmp)
			}
			wg.Done()
		}(int64(i))
	}
	wg.Wait()
	fmt.Println(cc)
}

func testaRand() {

}
