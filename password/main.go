package main

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

func main() {
	passwd := "Qwer_1234"
	passwddigest, err := Get("Qwer_1234")
	if err != nil {
		panic(err)
	}
	fmt.Println(passwddigest, Compare(passwddigest, passwd))
}

// Get returns the encrypted string for specified utils
func Get(str string) (string, error) {
	//TODO:Perhaps than sha256 performance slightly low;But it is more safe 20170609

	hash, err := bcrypt.GenerateFromPassword([]byte(str), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// Compare is to compare encryptStr and provided utils
func Compare(encryptStr, str string) bool {
	// Comparing the password with the hash
	return nil == bcrypt.CompareHashAndPassword([]byte(encryptStr), []byte(str))
}
