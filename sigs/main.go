package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"

	"github.com/filecoin-project/go-address"
	"github.com/filecoin-project/go-crypto"
)

type secpSigner struct{}

func (secpSigner) GenPrivate() ([]byte, error) {
	priv, err := crypto.GenerateKey()
	if err != nil {
		return nil, err
	}
	return priv, nil
}

func (secpSigner) ToPublic(pk []byte) ([]byte, error) {
	return crypto.PublicKey(pk), nil
}

func main() {
	s := secpSigner{}

	pv, err := s.GenPrivate()
	if err != nil {
		panic(err)
	}

	pk, err := s.ToPublic(pv)
	if err != nil {
		panic(err)
	}

	addr, _ := address.NewSecp256k1Address(pk)

	fmt.Println(address.NewSecp256k1Address(pk))
	fmt.Println(hex.EncodeToString(pv))

	k := Key{
		KeyInfo: KeyInfo{
			"secp256k1",
			pv,
		},
		PublicKey: pk,
		Address:   addr,
	}

	bb, _ := json.Marshal(k)

	ex := hex.EncodeToString(bb)
	fmt.Println(ex)

}

type KeyInfo struct {
	Type       string // secp256k1
	PrivateKey []byte
}

type Key struct {
	KeyInfo

	PublicKey []byte
	Address   address.Address
}
