#ifndef UCS_ECC_H
#define UCS_ECC_H

struct Point {
    long x;
    long y;
};


struct EncryptedData {
    struct Point point;
    long value;
};

struct Ecc;


// Api definition

// Create a new point
extern struct Point point_new(long x,long y);

// create a new Ecc which will be used for sign verify crypt and decrypt,
// NOTE: The result pointer to the structure Ecc must be freed at last.
extern struct Ecc * ecc_new();

// Free the memory to which the ecc point to.
extern void ecc_destroy(struct Ecc *ecc);

// Generate a private key.
// params:
// ecc: the pointer to the strcture Ecc
// buf: the buffer which store the private string.
// size: the size of the buf. must be large enough.
// return -1 if error occurs, else return the real size(byte) if the private string.
extern int ecc_generate_private(struct Ecc *ecc, char *buf, int size);

// genrate a public key
// params:
// ecc: the pointer to the structure Ecc
// private: the private string
// buf: the buffer which stores the public key string.
// size: the size of buf, must be large enough.
// return -1 if error occurs, else return the real size of the public key string.
extern int ecc_generate_public(struct Ecc *ecc, char *private, char *buf, int size);

// sign the data.
// params:
// ecc: pointer to the structure Ecc.
// private: the priavte key string.
// data: the data which will be signed.
// sign: return the sign value
// hash_value: return the hash value.
// return -1 if error occurs, else return 0.
extern int sign(struct Ecc *ecc, char *private, char *data, int *sign,
		int *hash_value);

// verify the data 
// params:
// ecc: pointer to the structure Ecc
// public: the public key string.
// data: the data string.
// sign: the sign value
// hash_value: hash value
// return -1 if failed, else return 0.
extern int verify(struct Ecc *ecc, char *public, char *data, int sign, int hash_value);


// encrypt the data.
// params:
// ecc: pointer to the strcuture Ecc.
// public: the public key string.
// data: the data to be encrypted.
// encrypted_data: store the result.
// return -1 if error occurs, else return 0.
extern int encrypt(struct Ecc *ecc, char *public, unsigned char data,
    struct EncryptedData* encrypted_data);

// decrypt the data.
// params:
// private: the private key string.
// data: store the original data.
// encrypted_data: encrypted data structure.
// return -1 if error occurs, else return 0;
extern int decrypt(struct Ecc *ecc, char *private, unsigned char *data,
    struct EncryptedData encrypted_data);


// return:
// -1: resolve private error
// -2: the length of the data to be encryped too larger(30000)
// -3: the param size must > 0
// -4: the param size too small.
extern int encrypt_string(struct Ecc *ecc, char *public, char *data, unsigned char *to_buf, int size);

// returns:
// -1: resolve the public error
// -2: the wrong format of encrypted_package.
// -3: the wrong header code in the encrypted_package
// -4: the length value in the encrypted_package not equal package_size
// -5: the wrong tailor code.
extern int decrypt_to_string(struct Ecc *ecc, char *private, unsigned char *encrypted_package, int package_size,
    char *to_buf, int buf_size);

// package a publick value to a string.
extern int package_public(long x, long y, char *buf, int size);

// resolve a public string to value.
extern int resolve_public(char *p,long *x,long *y);

// package private value to string.
extern int package_private(long value, char *buf, int size);

// resolve private string to value.
extern int resolve_private(char * p, long  *value);

#endif