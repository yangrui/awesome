#include <stdio.h>
#include "ucs_ecc.h"
#include <stdlib.h>
#include <inttypes.h>

char privates[512] = {"OGRkMGVkYmRmNDhlNzQzOWJjZTFkMzg5YWQ4YjdiMDEwMDAwMDAwMDAwMDUzZDYwYzA2ODk2MTI2N2NjMDVjNWFlMjE2NmVlOGY2NjI5ZTMwN2FlYTk4NGFiZTk4YTRiMDFiYTY3ZWU1NWI4OGRmNTMwMjgwNjZjMGM2ZWE0MGYxYmQ0MjEzYWNhZDUyMjJlYzZkNDhlMzRkMTI4MzZhNmE2MGM2NDU2ODZhOTk5MTAyZjljZDQwM2ZlMGU5YmQ4MTk0OWQ4NjliMjc0YzAzZWRhNDZkYzFkMjFhOWQxMjg0NmZlNGZjZmY3YjBlMzA3MzliNjQzNjdkZDgwNzI3Zg=="};

char mapAN(int k)
{
	static char *maps = "ABCDEFGHJKLMNOPQRSTUVWXYZ2345678";
	return maps[k % 32];
}
//签名数据
char base32[17] = {0};
//char data[50] = {0};
int test(char* data) {
// initiliaze the ecc.
	struct Ecc *ecc = ecc_new();

	int ret = 0;
	//printf("sign:  %s\n", data);

	// sign the data.
	//char *data = "Y2016E0001";
	int sign_value = 0;
	int hash_value = 0;
	ret = sign(ecc, privates, data, &sign_value, &hash_value);
	if (ret < 0) {
		printf("sign data error!\n");
		return -1;
	}
	printf("sign: %d  %d\n", sign_value,hash_value);

	uint64_t signEncry = sign_value;
	uint64_t hashEncry = hash_value;
	uint64_t bnEncry = (signEncry<<32)+hashEncry;

	uint64_t randEncry = rand();

	int i;
	for(i = 1;i<=16;i++)
	{
		//get right 5bits
		uint8_t base = bnEncry&(0x1f);

		base32[i-1] = mapAN(base);

		bnEncry = bnEncry>>5;
		//add rand num
		if(i==12)
		{
			bnEncry += (randEncry<<4);
		}
	}
	printf("sign data: %s\n", base32);

	// free the ecc.
	ecc_destroy(ecc);
}

int main( int argc, char *argv[]) {
    printf("argv: %s %d  %d %d \n",*argv, argc, sizeof(argv[0]), sizeof(argv[1]));
    if (argc < 2) {
        printf("缺少参数");
        return -1;
    }
    printf("%d %s \n", argc, argv[1]);
    char* c= argv[1];
    test(c);
    return 0;
}