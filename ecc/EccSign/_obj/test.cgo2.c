
#line 1 "cgo-builtin-prolog"
#include <stddef.h> /* for ptrdiff_t and size_t below */

/* Define intgo when compiling with GCC.  */
typedef ptrdiff_t intgo;

#define GO_CGO_GOSTRING_TYPEDEF
typedef struct { const char *p; intgo n; } _GoString_;
typedef struct { char *p; intgo n; intgo c; } _GoBytes_;
_GoString_ GoString(char *p);
_GoString_ GoStringN(char *p, int l);
_GoBytes_ GoBytes(void *p, int n);
char *CString(_GoString_);
void *CBytes(_GoBytes_);
void *_CMalloc(size_t);

__attribute__ ((unused))
static size_t _GoStringLen(_GoString_ s) { return (size_t)s.n; }

__attribute__ ((unused))
static const char *_GoStringPtr(_GoString_ s) { return s.p; }

#line 2 "/home/pi/go/src/EccSign/test.go"

#include <stdio.h>
#include "ucs_ecc.h"

#include <stdlib.h>
#include <inttypes.h>

char private[512] = {"OGRkMGVkYmRmNDhlNzQzOWJjZTFkMzg5YWQ4YjdiMDEwMDAwMDAwMDAwMDUzZDYwYzA2ODk2MTI2N2NjMDVjNWFlMjE2NmVlOGY2NjI5ZTMwN2FlYTk4NGFiZTk4YTRiMDFiYTY3ZWU1NWI4OGRmNTMwMjgwNjZjMGM2ZWE0MGYxYmQ0MjEzYWNhZDUyMjJlYzZkNDhlMzRkMTI4MzZhNmE2MGM2NDU2ODZhOTk5MTAyZjljZDQwM2ZlMGU5YmQ4MTk0OWQ4NjliMjc0YzAzZWRhNDZkYzFkMjFhOWQxMjg0NmZlNGZjZmY3YjBlMzA3MzliNjQzNjdkZDgwNzI3Zg=="};

char mapAN(int k)
{
static char *maps = "ABCDEFGHJKLMNOPQRSTUVWXYZ2345678";
return maps[k % 32];
}

int test() {
// initiliaze the ecc.
struct Ecc *ecc = ecc_new();

int ret = 0;

// sign the data.
char *data = "6a59-dd43-7ea5-e8f6";
int sign_value = 0;
int hash_value = 0;
ret = sign(ecc, private, data, &sign_value, &hash_value);
if (ret < 0) {
printf("sign data error!\n");
return -1;
}
printf("sign: %d  %d\n", sign_value,hash_value);

uint64_t signEncry = sign_value;
uint64_t hashEncry = hash_value;
uint64_t bnEncry = (signEncry<<32)+hashEncry;

uint64_t randEncry = rand();

//签名数据
char base32[17] = {0};
for(int i = 1;i<=16;i++)
{
//get right 5bits
uint8_t base = bnEncry&(0x1f);

base32[i-1] = mapAN(base);

bnEncry = bnEncry>>5;
//add rand num
if(i==12)
{
bnEncry += (randEncry<<4);
}
}
printf("sign data: %s\n", base32);

// free the ecc.
ecc_destroy(ecc);
}
#line 1 "cgo-generated-wrapper"


#line 1 "cgo-gcc-prolog"
/*
  If x and y are not equal, the type will be invalid
  (have a negative array count) and an inscrutable error will come
  out of the compiler and hopefully mention "name".
*/
#define __cgo_compile_assert_eq(x, y, name) typedef char name[(x-y)*(x-y)*-2+1];

/* Check at compile time that the sizes we use match our expectations. */
#define __cgo_size_assert(t, n) __cgo_compile_assert_eq(sizeof(t), n, _cgo_sizeof_##t##_is_not_##n)

__cgo_size_assert(char, 1)
__cgo_size_assert(short, 2)
__cgo_size_assert(int, 4)
typedef long long __cgo_long_long;
__cgo_size_assert(__cgo_long_long, 8)
__cgo_size_assert(float, 4)
__cgo_size_assert(double, 8)

extern char* _cgo_topofstack(void);

/*
  We use packed structs, but they are always aligned.
  The pragmas and address-of-packed-member are only recognized as warning
  groups in clang 4.0+, so ignore unknown pragmas first.
*/
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Waddress-of-packed-member"

#include <errno.h>
#include <string.h>


#define CGO_NO_SANITIZE_THREAD
#define _cgo_tsan_acquire()
#define _cgo_tsan_release()


#define _cgo_msan_write(addr, sz)

CGO_NO_SANITIZE_THREAD
void
_cgo_c89250318ace_Cfunc_test(void *v)
{
	struct {
		int r;
		char __pad4[4];
	} __attribute__((__packed__, __gcc_struct__)) *_cgo_a = v;
	char *_cgo_stktop = _cgo_topofstack();
	__typeof__(_cgo_a->r) _cgo_r;
	_cgo_tsan_acquire();
	_cgo_r = test();
	_cgo_tsan_release();
	_cgo_a = (void*)((char*)_cgo_a + (_cgo_topofstack() - _cgo_stktop));
	_cgo_a->r = _cgo_r;
	_cgo_msan_write(&_cgo_a->r, sizeof(_cgo_a->r));
}

